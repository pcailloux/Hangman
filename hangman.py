
from english_words import english_words_lower_alpha_set
import random
import re


class Hangman:

    def __init__(self, name, difficulty, dictionary,word=None) -> None:
        self.name = name
        self.difficulty = difficulty
        self.dictionary = dictionary
        if word == None:
            self.word = random.choice(
            [n for n in self.dictionary if len(n) == self.difficulty["word_length"]])
        else:
            self.word=word
        self.remainningTries = self.difficulty["tries"]
        self.displayline = ["_ " for n in range(len(self.word))]
        self.usedLetter = []
        self.found=False
        self.chars = {char for char in self.word}

    def isValidInput(self, inputToCheck):
            if re.findall("[a-z]", inputToCheck) and len(inputToCheck) == 1:
                if (inputToCheck in self.usedLetter):
                    print("you already tried this letter")
                    print("here is the list of letter you already picked\n",
                        *self.usedLetter)
                    return False
                return True                        
            else:
                print("'",inputToCheck, "' is not a valid input please enter a single letter ",sep="")
                return False



    def checkLetterInWord(self, letter: str):
        self.usedLetter.append(letter)
        if letter in self.chars:
            print("nice one, you found a letter")
            #  "_ " , "_ "
            for i in range(len(self.word)):
                if letter == self.word[i]:
                    self.displayline[i]=letter+ " "
            if "_ " not in self.displayline:
                self.found=True
        else:
            print("too bad ", letter, " is not present")
            self.remainningTries = self.remainningTries - 1
            print(self.remainningTries, "shots left")
        print(*self.displayline)


difficulties = {
    "easy":  {
        "word_length": 5,
        "tries": 10
    },
    "normal": {
        "word_length": 8,
        "tries": 8,
    },
    "hard":
    {
        "word_length": 12,
        "tries": 7
    },
}

myhangman = Hangman(
    "Pierre", difficulties["easy"], english_words_lower_alpha_set)
my_word = myhangman.word
print([n for n in my_word])
 
while ((myhangman.found == False) and (myhangman.remainningTries > 0)):
    letter = input("please chosse a letter: ").lower()
    if myhangman.isValidInput(letter):
        myhangman.checkLetterInWord(letter)
if myhangman.found==True:
    print("Congratulations",myhangman.name,"you won !!\n"
        # ,"it took you" , myhangman.difficulty["tries"]-myhangman.remainningTries , "to guess the magic word "          
                                                        )
        
